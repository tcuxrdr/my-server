import path from 'path'
import express from 'express';
import { router } from './mainServer/routes/routes.js'
import { fileURLToPath } from "url";
import mongoose from "mongoose";

import dotenv from 'dotenv'
dotenv.config();
// import CourseRates from './currencyRates/curencyRates';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
const app = express();
const PORT = process.env.MAIN_SERVER_PORT || 5000;
const DB_URL = process.env.DB_URL;

async function startApp(){
	try {
		await mongoose.connect(DB_URL, { useNewUrlParser: true, useUnifiedTopology: true })
		router(app);

    app.listen(PORT, () => {
      console.log(`Application listening on port ${PORT}`);
    });
	} catch (err) {
		console.log(err)
	}
}

startApp()




// app.use(express.static(path.join(__dirname, 'public')))

// console.log(path.join(__dirname, "public"));

// const http = require('http');


// const server = http.createServer((req, res) => {
// 	if(req.url === '/users') {
// 		return res.end('USERS')
// 	}
// 	if(req.url === '/posts') {
// 		return res.end('POSTS')
// 	}

// 	res.end(req.url)
// });

// server.listen(PORT, () => console.log(`Server started on PORT ${PORT}`))