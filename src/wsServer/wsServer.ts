import express from 'express';
import { Server } from "socket.io";
import { createServer } from "http";
import cors from 'cors'

const app = express()
app.use(cors())

const server = createServer(app);
const io = new Server(server, {
  cors: {
    origin: "http://localhost:3000",
    methods: ["GET", "POST"],
  },
});

app.get("/", (req, res) => {
  res.send({ message: "wsServer" });
});

io.on("connection", (socket) => {
  console.log(`user connectet on ${socket.id}`)

  socket.on("send_message", (data) => {
    console.log(data)
  })
  setInterval(() => {
    socket.emit("to_front", { key1: "value1", key2: "value2" })
    console.log(new Date())
  ;}, 5000)
  
})


server.listen(4002, () => {
	console.log('Server started on 4002')
})



