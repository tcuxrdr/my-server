import axios from 'axios';
import mongoose from 'mongoose';
import aguid from '@pratikpc/aguid'
import RatesSchema from '../currencyRates/curencyRatesSchema'


interface BankCurrencyRatesDTO {
  r030: number;
  txt: string;
  rate: number;
  cc: string;
  exchangedate: string;
}

interface ICurrency {
	rate: number;
	symbol: string;
	date: string;
}

type CreateCurrencyDTO = Omit<ICurrency, "id">;

export class CurrencyRatesProcessor {
  constructor(id: string, intervalInSec: number = 60000) {
    this.id = id;
    this.intervalInSec = intervalInSec;
  }

  private id: string;
  private intervalInSec: number;

  async start(): Promise<void> {
    setInterval(async () => {
      await this.saveToDB();
    }, this.intervalInSec);
  }

  async saveToDB() {
    // throw new Error('Is not implemented');
    const array = await this.getCurrencyArray();
    const cur = this.getCurrency(array, "USD");
    if (!cur) {
      console.error("It's not currency");
      return;
    }
    RatesSchema.create(cur)
    console.log({ [this.id]: cur });
  }

  private async getCurrencyArray(): Promise<BankCurrencyRatesDTO[]> {
    const response = await axios.get(
      "https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json"
    );
    return response.data;
  }

  private getCurrency(
    currencyArray: BankCurrencyRatesDTO[],
    symbol: string
  ): ICurrency {
    // const guid = aguid();
    const date = new Date().toISOString();
    const currency = currencyArray.find((item) => item.cc === symbol);
    return (
      currency && {
        rate: currency.rate,
        symbol: currency.cc,
        date,
      }
    );
  };
};

// new CurrencyRatesProcessor("1").start();
// new CurrencyRatesProcessor("2", 10000).start();

