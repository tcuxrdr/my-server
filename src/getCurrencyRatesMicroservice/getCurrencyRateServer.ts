import express from 'express';
import mongoose from 'mongoose';
import { CurrencyRatesProcessor } from './getCurrencyRateMicroservice';
import cors from 'cors';
import dotenv from 'dotenv';
import ws from 'ws';
// import redis from 'redis';
dotenv.config()

const app = express();
const PORT = process.env.MICROSERVICE_PORT;
const DB_URL = process.env.DB_URL;
// const publisher = redis.createClient({
// 	url: "http://localhost:6379",
// });
app.use(cors())

async function startServer() {
	try{
		await mongoose.connect(DB_URL)

		new CurrencyRatesProcessor("Create data in DB", 300000).start();

    app.get("/", (req, res) => {
      res.send({ message: "Hello I'm microservice" });
    });

		// (async () => {
    //   const article = {
    //     id: "123456",
    //     name: "Using Redis Pub/Sub with Node.js",
    //     blog: "Logrocket Blog",
    //   };

    //   await publisher.connect();

    //   await publisher.publish("article", JSON.stringify(article));
    // })();

		app.listen(PORT, () => {
			console.log(`Get currency rates microservice started on PORT: ${PORT}`)
		})

		
	} catch(err) {
		console.log(err)
	}
}

startServer()
