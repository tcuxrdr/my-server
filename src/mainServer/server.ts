import express from "express";
import { router } from "./routes/routes";
import mongoose from "mongoose";
import cors from 'cors'
import { Server } from "socket.io";
import { createServer } from "http";
// import redis from 'redis'

import dotenv from "dotenv";
dotenv.config();

const app = express();
app.use(cors())
const PORT = process.env.MAIN_SERVER_PORT || 5000;
const DB_URL = process.env.DB_URL;
const httpServer = createServer(app);
const io = new Server(httpServer, {
  cors: {
    origin: "http://localhost:3000",
    methods: ["GET", "POST"]
  }
});

async function startApp() {
  try {
    await mongoose.connect(DB_URL);
    router(app);  

    // (async () => {
    //   const client = redis.createClient();

    //   const subscriber = client.duplicate();

    //   await subscriber.connect();

    //   await subscriber.subscribe("article", (message) => {
    //     console.log(message); // 'message'
    //   });
    // })();

    httpServer.listen(PORT, () => {
      console.log(`Application listening on port ${PORT}`);
    });
  } catch (err) {
    console.log(err);
  }
}


startApp();
