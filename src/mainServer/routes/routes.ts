import { Request, Response, Express } from 'express';

const users = [
  {
    name: "Ivan",
    age: 38,
  },
  {
    name: "Petr",
    age: 23,
  },
  {
    name: "Hodor",
    age: 16,
  },
  {
    name: "Mortis",
    age: 45,
  },
];

const router = (app: Express): void => {
  app.get("/", (req, res) => {
    res.send({ message: "Hello world!" });
  });


  app.get("/users", (req, res) => {
    res.send(JSON.stringify(users));
  });

  app.post('/login', (req: Request, res: Response) => {
    const auth = req.headers.authorization; // Authorization: login:password
    if(!auth){
      throw new Error('Ne auth')
    }
    const [login, password] = auth.split(':')
    if(login !='test' || password != 'test') {
      throw new Error(`Unknown user - login: ${login}, password: ${password}`)
    } 
    res.send({isLogged: true})
  })
}

export {router};
