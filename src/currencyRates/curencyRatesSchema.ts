import mongoose from "mongoose";

const RatesSchema = new mongoose.Schema({
  rate: { type: Number, required: true },
  symbol: { type: String, required: true },
  date: { type: String, required: true },
});

export default mongoose.model("RatesSchema", RatesSchema);
