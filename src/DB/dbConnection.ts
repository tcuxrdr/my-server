import mongoose from 'mongoose';
import dotenv from 'dotenv'
dotenv.config()

async function connectToDB(dbURL: string): Promise<void> {
  await mongoose.connect(dbURL);
}

export default connectToDB;